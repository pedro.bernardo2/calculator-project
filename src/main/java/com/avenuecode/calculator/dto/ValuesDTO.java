package com.avenuecode.calculator.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ValuesDTO {
	
	private BigDecimal firstValue;
	
	private BigDecimal secondValue;

}
