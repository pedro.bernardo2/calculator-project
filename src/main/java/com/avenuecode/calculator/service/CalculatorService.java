package com.avenuecode.calculator.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.avenuecode.calculator.dto.ValuesDTO;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CalculatorService {
	
	public BigDecimal multiply (ValuesDTO valuesDTO) {
		BigDecimal result =  valuesDTO.getFirstValue().multiply(valuesDTO.getSecondValue());
		return result;
	}

}
