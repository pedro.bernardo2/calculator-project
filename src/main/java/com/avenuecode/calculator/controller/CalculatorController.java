package com.avenuecode.calculator.controller;

import java.math.BigDecimal;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.avenuecode.calculator.dto.ValuesDTO;
import com.avenuecode.calculator.service.CalculatorService;

import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping()
@RequiredArgsConstructor
public class CalculatorController {
	
	private final CalculatorService calculatorService;
	
	@PostMapping(value = "/multiply", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BigDecimal> multiply(@RequestBody ValuesDTO valueDTO) {
		return ResponseEntity.ok(calculatorService.multiply(valueDTO));
	}

}
