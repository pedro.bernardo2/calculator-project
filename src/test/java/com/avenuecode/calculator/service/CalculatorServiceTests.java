package com.avenuecode.calculator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.avenuecode.calculator.dto.ValuesDTO;

@SpringBootTest
public class CalculatorServiceTests {
	
	@Autowired
	private CalculatorService calculatorService;
	
	@Test
	public void isMultiplyShouldReturnTen() {
		ValuesDTO valuesDTO = new ValuesDTO();
		valuesDTO.setFirstValue(new BigDecimal("2.00"));
		valuesDTO.setSecondValue(new BigDecimal("5.00"));
		BigDecimal result = calculatorService.multiply(valuesDTO);
		assertEquals(result, new BigDecimal("10.0000"));
	}

}
