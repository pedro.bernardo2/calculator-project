package com.avenuecode.calculator.controller;

import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.avenuecode.calculator.dto.ValuesDTO;
import com.avenuecode.calculator.service.CalculatorService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = CalculatorController.class)
public class CalculatorControllerTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CalculatorService calculatorService;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Test
	public void multiplyShouldReturnFifty() throws Exception {
		when(calculatorService.multiply(any(ValuesDTO.class))).thenReturn(new BigDecimal("50.00"));
		ValuesDTO valuesDTO = new ValuesDTO();
		valuesDTO.setFirstValue(new BigDecimal("2.00"));
		valuesDTO.setSecondValue(new BigDecimal("10.00"));
		BigDecimal result = new BigDecimal("50.00");
		String jsonResult = objectMapper.writeValueAsString(result);
		String jsonValuesDTO = objectMapper.writeValueAsString(valuesDTO);
			
		mockMvc.perform(
				post("/multiply")
				.contentType(MediaType.APPLICATION_JSON)
				.content(jsonValuesDTO))
				.andExpect(content().json(jsonResult));
	}
	

}
